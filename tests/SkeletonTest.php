<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Uello\JsonSkeleton\Skeleton;

class SkeletonTest extends TestCase
{
    public function testResultMapString()
    {
        $map = [
            'foo' => [
                'bar' => 'baz',
                'qux' => 'quxx'
            ]
        ];

        $skeleton = new Skeleton($map);
        $skeleton->setData([
            'baz'  => 1,
            'quxx' => 2
        ]);

        $result   = $skeleton->getResult();
        $expected = [
            "foo" => [
                "bar" => 1,
                'qux' => 2
            ]
        ];

        $this->assertEquals($expected, $result);
    }

    public function testResultFixed()
    {
        $map = [
            "foo" => [
                "bar" => [
                    "value" => "baz",
                    "type"  => "fixed"
                ]
            ]
        ];

        $result   = (new Skeleton($map))->getResult();
        $expected = [
            "foo" => [
                "bar" => "baz"
            ]
        ];

        $this->assertEquals($expected, $result);
    }

    public function testResultNotFixed()
    {
        $number = mt_rand(0, 9999);
        $map    = [
            "foo" => [
                "bar" => [
                    "value" => "baz",
                    "type"  => "int"
                ]
            ]
        ];

        $skeleton = new Skeleton($map);
        $skeleton->set('baz', $number);

        $result   = $skeleton->getResult();
        $expected = [
            "foo" => [
                "bar" => $number
            ]
        ];

        $this->assertEquals($expected, $result);
        $this->assertIsInt($expected['foo']['bar']);
    }

    public function testResultDatetimeWithFormat()
    {
        $map    = [
            "foo" => [
                "bar" => [
                    "value"  => "baz",
                    "type"   => "datetime",
                    "format" => 'd/m/Y H:i:s'
                ]
            ]
        ];

        $skeleton = new Skeleton($map);
        $skeleton->set('baz', new \DateTime('2022-08-21 14:10:00'));

        $result   = $skeleton->getResult();
        $expected = [
            "foo" => [
                "bar" => '21/08/2022 14:10:00'
            ]
        ];

        $this->assertEquals($expected, $result);
    }

    public function testResultWithFixedAndReplace()
    {
        $map = [
            'foo' => [
                'bar' => [
                    'type'    => 'fixed',
                    'value'   => 'baz/:baz/:qux',
                    'replace' => [
                        ':baz' => 1,
                        ':qux' => 2
                    ]
                ]
            ]
        ];

        $result   = (new Skeleton($map))->getResult();
        $expected = [
            "foo" => [
                "bar" => "baz/1/2"
            ]
        ];

        $this->assertEquals($expected, $result);
    }

    public function testResultWithReplace()
    {
        $map = [
            'foo' => [
                'bar' => [
                    'value'   => 'baz',
                    'replace' => [
                        ':baz' => 1,
                        ':qux' => 2
                    ]
                ]
            ]
        ];

        $skeleton = new Skeleton($map);
        $skeleton->set('baz', 'baz/:baz/:qux');

        $result   = $skeleton->getResult();
        $expected = [
            "foo" => [
                "bar" => "baz/1/2"
            ]
        ];

        $this->assertEquals($expected, $result);
    }

    public function testResultWithConditionalAndReplace()
    {
        $map = [
            'foo' => [
                'bar' => [
                    'value'   => 'baz',
                    "conditional" => [
                        [
                            "if"   => "\$baz == \"qux\"", 
                            "then" => "baz/:baz"
                        ]
                    ],
                    'replace' => [
                        ':baz' => 1,
                        ':qux' => 2
                    ]
                ]
            ]
        ];

        $skeleton = new Skeleton($map);
        $skeleton->set('baz', 'qux');

        $result   = $skeleton->getResult();
        $expected = [
            "foo" => [
                "bar" => "baz/1"
            ]
        ];

        $this->assertEquals($expected, $result);
    }

    public function testResultWithConditionalDefaultAndReplace()
    {
        $map = [
            'foo' => [
                'bar' => [
                    'value'   => 'baz',
                    "conditional" => [
                        [
                            "if"   => "\$baz == \"qux\"", 
                            "then" => "baz/:baz"
                        ]
                    ],
                    'replace' => [
                        ':baz' => 1,
                        ':qux' => 2
                    ]
                ]
            ]
        ];

        $skeleton = new Skeleton($map);
        $skeleton->set('baz', 'qux/:qux');

        $result   = $skeleton->getResult();
        $expected = [
            "foo" => [
                "bar" => "qux/2"
            ]
        ];

        $this->assertEquals($expected, $result);
    }

    public function testResultWithConditional()
    {
        $map = [
            "foo" => [
                "bar" => [
                    "value"       => "baz",
                    "conditional" => [
                        [
                            "if"   => "\$baz == \"foo\" && \$status == 1", 
                            "then" => "Testing"
                        ]
                    ]
                ]
            ]
        ];

        $skeleton = new Skeleton($map);
        $skeleton->set('baz', "foo");
        $skeleton->set('status', 1);

        $result   = $skeleton->getResult();
        $expected = [
            "foo" => [
                "bar" => "Testing"
            ]
        ];

        $this->assertEquals($expected, $result);
    }

    public function testResultWithTwoConditional()
    {
        $map = [
            "foo" => [
                "bar" => [
                    "value"       => "baz",
                    "conditional" => [
                        [
                            "if"   => "\$baz == \"foo\" && \$status == 1", 
                            "then" => "Testing"
                        ],
                        [
                            "if"   => "\$baz == \"foo\" && \$status != 1", 
                            "then" => "Tested"
                        ]
                    ]
                ]
            ]
        ];

        $skeleton = new Skeleton($map);
        $skeleton->set('baz', "foo");
        $skeleton->set('status', 2);

        $result   = $skeleton->getResult();
        $expected = [
            "foo" => [
                "bar" => "Tested"
            ]
        ];

        $this->assertEquals($expected, $result);
    }

    public function testResultWithDefault()
    {
        $map = [
            "foo" => [
                "bar" => [
                    "value"       => "baz",
                    "conditional" => [
                        [
                            "if" => "\$baz == \"foo\" && \$status == 1", 
                            "then" => "Testing"
                        ],
                        [
                            "if" => "\$baz != \"foo\" && \$status != 1", 
                            "then" => "Tested"
                        ]
                    ]
                ]
            ]
        ];

        $skeleton = new Skeleton($map);
        $skeleton->set('baz', 'foo');
        $skeleton->set('status', 2);

        $result   = $skeleton->getResult();
        $expected = [
            "foo" => [
                "bar" => "foo"
            ]
        ];

        $this->assertEquals($expected, $result);
    }

    public function testResultWithTwoFields()
    {
        $map = [
            "foo" => [
                "bar" => [
                    "value"       => "baz",
                    "conditional" => [
                        [
                            "if"   => "\$baz != \"foo\" && \$status == 1", 
                            "then" => "Testing"
                        ],
                        [
                            "if"   => "\$baz != \"foo\" && \$status != 1", 
                            "then" => "Tested"
                        ]
                    ]
                ],
                "baz" => [
                    "value"       => "status",
                    "conditional" => [
                        [
                            "if" => "\$status == 2", 
                            "then" => ""
                        ],
                    ]
                ]
            ]
        ];

        $skeleton = new Skeleton($map);
        $skeleton->set('baz', 'foo');
        $skeleton->set('status', 2);

        $result   = $skeleton->getResult();
        $expected = [
            "foo" => [
                "bar" => "foo",
                "baz" => ""
            ]
        ];

        $this->assertEquals($expected, $result);
    }
}