<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Uello\JsonSkeleton\Callback;

class CallbackTest extends TestCase
{
    public function testFillLeft()
    {
        $result = Callback::fillLeft("json", 8, "-");

        $this->assertEquals("----json", $result);
    }
}