<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Uello\JsonSkeleton\Types;

class TypesTest extends TestCase
{
    /**
     * @dataProvider types
     */
    public function testApplySuccess($expected, $type, $value, $format = null)
    {
        $result = Types::apply($type, $value, $format);

        $this->assertEquals($expected, $result);
    }

    public function types()
    {
        return [
            'bool-true'   => [true, Types::BOOLEAN, 1],
            'bool-false'  => [false, Types::BOOLEAN, 0],
            'int'         => [99, Types::INTEGER, "99"],
            'int-float'   => [88, Types::INTEGER, "88.88"],
            'float'       => [88.88, Types::FLOAT, "88.88"],
            'string'      => ["88.88", Types::STRING, 88.88],
            'date-format' => ['01/07/2021 15:03:01', Types::DATE_TIME, new \DateTime('2021-07-01T15:03:01'), 'd/m/Y H:i:s'],
            'file'        => ["JsonSkeleton", Types::FILE, __DIR__ . "/mocks/types.txt"],
            'base64'      => [base64_encode("JsonSkeleton"), Types::STRING, "JsonSkeleton", Types::FORMAT_BASE64],
        ];
    }
}