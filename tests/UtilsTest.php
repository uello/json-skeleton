<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Uello\JsonSkeleton\Utils;

class UtilsTest extends TestCase
{

    public function testArrayMergeRecursive()
    {
        $first = [
            "foo" => [
                "bar" => 1,
                "baz" => 1
            ]
        ];

        $second = [
            "foo" => [
                "bar" => 2
            ]
        ];

        $expected = [
            "foo" => [
                "bar" => 2,
                "baz" => 1
            ]
        ];

        $result = Utils::arrayMergeRecursive($first, $second);

        $this->assertEquals($expected, $result);
    }
}