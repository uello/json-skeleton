<?php

use PHPUnit\Framework\TestCase;
use Uello\JsonSkeleton\Conditional;

class ConditionalTest extends TestCase
{
    public function testEvaluateTrue()
    {
        $conditional = new Conditional([
            'a' => 'foo',
            'b' => 1
        ]);

        $result = $conditional->evaluate('$a == "foo" && $b == 1');

        $this->assertTrue($result);
    }

    public function testEvaluateFalse()
    {
        $conditional = new Conditional([
            'a' => 'foo',
            'b' => 0
        ]);

        $result = $conditional->evaluate('$a == "foo" && $b == 1');

        $this->assertNotTrue($result);
    }
}