<?php

namespace Uello\JsonSkeleton;

class Conditional
{
    private $params;

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    public function evaluate(string $cond): ?bool
    {
        extract($this->params);

        $return = null;

        $expr = sprintf('$return = %s;', $cond);
        
        eval($expr);
        
        return $return;
    }
}