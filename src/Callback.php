<?php

namespace Uello\JsonSkeleton;

class Callback
{
    public static function fillLeft($base, $size, $fillString)
    {
        return str_pad($base, $size, $fillString, STR_PAD_LEFT);
    }
}