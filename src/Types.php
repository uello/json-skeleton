<?php

namespace Uello\JsonSkeleton;

class Types
{
    const DATE_TIME     = 'datetime';
    const STRING        = 'string';
    const INTEGER       = 'int';
    const FLOAT         = 'float';
    const BOOLEAN       = 'boolean';
    const FIXED         = 'fixed';
    const FILE          = 'file';
    const FORMAT_BASE64 = 'base64';

    public static function apply(string $type = null, $value, $format = null)
    {
        switch ($type) {
            case self::BOOLEAN:
                $value = (bool) $value;
            break;
            case self::INTEGER:
                $value = (int) $value;
            break;
            case self::FLOAT:
                $value = (float) $value;
            break;
            case self::DATE_TIME:
                if (!$value instanceof \DateTime) {
                    $value = new \DateTime($value);
                }

                $value = $value->format($format ?? 'Y-m-d H:i:s');
            break;
            case self::STRING:
                $value = (string) $value;
            break;
            case self::FILE:
                $value = file_get_contents($value);
            break;
        }

        return ($format == self::FORMAT_BASE64) ? base64_encode($value) : $value;
    }
}