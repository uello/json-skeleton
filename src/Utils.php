<?php
namespace Uello\JsonSkeleton;

class Utils
{
    public static function arrayMergeRecursive()
    {
        $arrays = func_get_args();
        $base   = array_shift($arrays);

        if (empty($base)) {
            return end($arrays);
        }

        foreach ($arrays as $array) {
            reset($base);

            foreach ($array as $key => $value) {
                if (is_array($value) && is_array($base[$key] ?? '')) {
                    $base[$key] = self::arrayMergeRecursive($base[$key], $value);
                } else {
                    $base[$key] = $value;
                }
            }
        }

        return $base;
    }
}