<?php

namespace Uello\JsonSkeleton;

class Skeleton
{
    protected $skeleton;
    private $skeletonMap;
    private $result;
    private $params = [];

    public function __construct(array $skeleton)
    {
        $this->skeleton = $skeleton;
        $this->result   = [];
        $this->map();
    }

    public function map($skeleton = null, $map = [])
    {
        $skeleton = $skeleton ?? $this->skeleton;

        foreach ($skeleton as $skeletonKey => $skeletonObj) {
            if (
                !isset($skeletonObj['value'])
                && is_array($skeletonObj)
            ) {
                $childMap   = $map;
                $childMap[] = $skeletonKey;
                $this->map($skeletonObj, $childMap);

                continue;
            }

            $value       = $skeletonObj['value'] ?? $skeletonObj;
            $type        = $skeletonObj['type'] ?? Types::STRING;
            $key         = $type == Types::FIXED ? $skeletonKey : $value;
            $format      = $skeletonObj['format'] ?? null;
            $callback    = $skeletonObj['callback'] ?? null;
            $replace     = $skeletonObj['replace'] ?? null;
            $conditional = $skeletonObj['conditional'] ?? null;

            $thisMap   = $map;
            $thisMap[] = $skeletonKey;

            $this->skeletonMap[] = [
                'column'      => $key,
                'map'         => $thisMap,
                'type'        => $type,
                'format'      => $format,
                'value'       => $value,
                'callback'    => $callback,
                'replace'     => $replace,
                'conditional' => $conditional
            ];
        }
    }

    public function set(string $key, $value, $allowFixed = false)
    {
        $this->params[$key] = compact('value', 'allowFixed');
    }

    private function evaluate()
    {
        foreach ($this->params as $key => $val) {
            $allowFixed = $val['allowFixed'];
            $value      = $val['value'];

            $map = array_filter($this->skeletonMap, function ($map) use ($key, $allowFixed) {
                return $map['column'] == $key && ($map['type'] != Types::FIXED || $allowFixed);
            });

            if (empty($map)) {
                continue;
            }

            foreach ($map as $key => $mapKey) {
                $reverse = array_reverse($mapKey['map']);
                $value   = $mapKey['type'] == Types::FIXED ? $mapKey['value'] : $value;
                $value   = Types::apply($mapKey['type'], $value, $mapKey['format']);
                $set     = [];

                foreach (($mapKey['callback'] ?? []) as $callback) {
                    $value = Callback::{$callback['function']}($value, $callback['params'][1], $callback['params'][0]);
                }

                $value = $this->replace($mapKey, $value);

                foreach ($reverse as $key => $skeletonKey) {
                    $set = ($key == 0) ? [$skeletonKey => $value] : [$skeletonKey => $set];
                }

                $this->result = Utils::arrayMergeRecursive($this->result, $set);
            }
        }
    }

    private function replace(array $map, $value)
    {
        $replacements = $map['replace'] ?? [];

        if (empty($replacements)) {
            return $value;
        }

        foreach ($replacements as $replace => $val) {
            $value = str_replace($replace, $this->params[$val]['value'] ?? $val, $value);
        }

        return $value;
    }

    private function evaluateBySkeletonMap()
    {
        $params = array_map(function ($param) {
            return $param['value'];
        }, $this->params);

        foreach ($this->skeletonMap as $map) {
            if (!isset($map['conditional'])) {
                continue;
            }

            $result      = null;
            $conditional = new Conditional($params);

            foreach ($map['conditional'] as $condition) {
                $evaluated = $conditional->evaluate($condition['if']);

                if ($evaluated) {
                    $result = $then = $condition['then'];

                    if (empty($then)) {
                        break;
                    }

                    $trimmed = trim($then);
                    $value   = $this->params[substr($trimmed, 1)]['value'] ?? null;

                    if ($trimmed[0] == '$') {
                        $result = $value;
                    }

                    break;
                }
            }

            if ($result === null) {
                continue;
            }

            $result  = $this->replace($map, $result);
            $set     = [];
            $reverse = array_reverse($map['map']);

            foreach ($reverse as $key => $skeletonKey) {
                $set = ($key == 0) ? [$skeletonKey => $result] : [$skeletonKey => $set];
            }

            $this->result = Utils::arrayMergeRecursive($this->result, $set);
        }
    }

    public function setData(array $data)
    {
        foreach ($data as $key => $value) {
            $this->set($key, $value);
        }
    }

    public function getResult($reset = false): array
    {

        $fixed = array_filter($this->skeletonMap, function ($map) {
            return $map['type'] == Types::FIXED;
        });

        foreach ($fixed as $f) {
            $this->set($f['column'], $f['value'], true);
        }

        $this->evaluate();
        $this->evaluateBySkeletonMap();

        $result = $this->result;

        if ($reset) {
            $this->reset();
        }

        return $result;
    }

    public function reset()
    {
        $this->result = [];
    }
}
